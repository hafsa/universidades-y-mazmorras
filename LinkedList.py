

class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None

    def __str__(self):
        return str(self.data)

#node_1 = Node(1)
#node_2 = Node(2)
#node_1.next = node_2
#node_3 = Node(3)
#node_2.next = node_3
#print(node_1.next, node_2.next, node_3.next)
#if node_3.next:
#    print(node_3.next)
#else:
#    print("Fin de la estructura")


class LinkedList:

    def __init__(self):
        self.head = None

    def add_first(self, data):
        if not self.head:
            self.head = Node(data)
        else:
            new_node = Node(data)
            new_node.next = self.head
            self.head = new_node

    def add_last(self, data):
        if not self.head:
            self.head = Node(data)
        else:
            n = self.head
            while n.next != None:
                n = n.next
            n.next = Node(data)

    def find(self, data):
        n = self.head
        if not n:
            return None
        else:
            node_found = None
            found = False
            while n != None and not found:
                if n.data == data:
                    node_found = n.data
                    found = True
                n = n.next
            return node_found

    def find_all(self, data):
        n = self.head
        if not n:
            return None
        else:
            list_results = []
            while n != None:
                if n.data == data:
                    list_results.append(n.data)
                n = n.next
            return list_results

    def delete(self, data):
        if self.head is None:
            print("List is empty so no element was deleted.")
        else:
            n = self.head
            n_prev = None
            found = False
            while n != None and not found:
                if n.data == data:
                    if n_prev is None:
                        self.head = n.next
                    elif n.next is None:
                        n_prev.next = None
                    else:
                        n_prev.next = n.next
                    found = True
                n_prev = n
                n = n.next

    @staticmethod
    def iterate_backwards(node):
        if node.next:
            LinkedList.iterate_backwards(node.next)
        print(node.data)

    def print_backwards_v2(self):
        if self.head is None:
            print("List is empty.")
        else:
            LinkedList.iterate_backwards(self.head)

    def print_backwards(self):
        if self.head is None:
            print("List is empty")
        else:
            new_list = []
            n = self.head
            while n != None:
                new_list.append(n.data)
                n = n.next
            for e in reversed(new_list):
                print(e)

    def delete_all(self, data):
        if self.head is None:
            print("List is empty so no element was deleted.")
        else:
            n = self.head
            n_prev = None
            found = False
            while n != None:
                if n.data == data:
                    if n_prev is None:
                        self.head = n.next
                    elif n.next is None:
                        n_prev.next = None
                    else:
                        n_prev.next = n.next
                n_prev = n
                n = n.next

    def print_linked_list(self):
        n = self.head
        if not n:
            print("Linked list is empty")
        else:
            while n != None:
                print(n.data)
                n = n.next

    def size(self):
        count = 0
        if self.head != None:
            n = self.head
            while n != None:
                count += 1
                n = n.next
        return count


class Student:
    def __init__(self, name, nexp):
        self.name = name
        self.nexp = nexp

    def __str__(self):
        return self.name + " " + str(self.nexp)

    def __eq__(self, nexp):
        return self.nexp == nexp


# %%

#l = LinkedList()
#l.add_first(Student("David", 20))
#l.add_first(Student("Juana", 15))
#l.add_last(Student("Sergio", 10))
#l.print_linked_list()

#highest_student = None
#n = l.head
#if not n:
#    print("Linked list is empty")
#else:
#    while n != None:
#        if highest_student is None or n.data.nexp > highest_student.nexp:
#            highest_student = n.data
#        n = n.next
#    print("Highest:", highest_student)

# %%

#l_n = LinkedList()
#l_n.add_last(1)
#l_n.add_last(2)
#l_n.add_last(3)
#l_n.add_last(4)
#l_n.add_last(3)
#l_n.head = None
#l_n.print_linked_list()
#"""result = l_n.find(4)
##l_n.delete_all(3)
#print("--- BACKWARDS ---")
#l_n.print_backwards_v2()
#l_n.head = None"""

# %%

#l = LinkedList()
#l.add_first(Student("David", 20))
#l.add_first(Student("Juana", 15))
#l.add_last(Student("David", 20))
#l.print_linked_list()
#result = l.find_all(20)
#print("-----FOUND -----")
#for s in result:
#    print(s)


# %%

def calculate_average(l):
    n = l.head
    addition = 0
    while n != None:
        addition += n.data
        n = n.next
    return addition / l.size()


#exit = False
#l = LinkedList()
#while not exit:
#    try:
#        n = input("Enter a number: ")
#        n = float(n)
#        l.add_last(n)
#    except ValueError:
#        if n.lower() == "exit":
#            exit = True
#        else:
 #           print("Enter a number or exit to finish.", end="")

# calculate average here
#if l.size() > 0:
#    print("Average: ", calculate_average(l))
#else:
#    print("No number were provided.")


# %%

class PrivateLinkedList:
    def __init__(self):
        self.__head = None

    def add_first(self, data):
        if not self.__head:
            self.__head = Node(data)
        else:
            new_node = Node(data)
            new_node.next = self.__head
            self.__head = new_node

    def add_last(self, data):
        if not self.__head:
            self.__head = Node(data)
        else:
            n = self.__head
            while n.next != None:
                n = n.next
            n.next = Node(data)

    def find(self, data):
        n = self.__head
        if not n:
            return None
        else:
            node_found = None
            found = False
            while n != None and not found:
                if n.data == data:
                    node_found = n.data
                    found = True
                n = n.next
            return node_found

    def find_all(self, data):
        n = self.__head
        if not n:
            return None
        else:
            list_results = []
            while n != None:
                if n.data == data:
                    list_results.append(n.data)
                n = n.next
            return list_results

    def delete(self, data):
        if self.__head is None:
            print("List is empty so no element was deleted.")
        else:
            n = self.__head
            n_prev = None
            found = False
            while n != None and not found:
                if n.data == data:
                    if n_prev is None:
                        self.__head = n.next
                    elif n.next is None:
                        n_prev.next = None
                    else:
                        n_prev.next = n.next
                    found = True
                n_prev = n
                n = n.next

    @staticmethod
    def iterate_backwards(node):
        if node.next:
            LinkedList.iterate_backwards(node.next)
        print(node.data)

    def print_backwards_v2(self):
        if self.__head is None:
            print("List is empty.")
        else:
            LinkedList.iterate_backwards(self.__head)

    def print_backwards(self):
        if self.__head is None:
            print("List is empty")
        else:
            new_list = []
            n = self.__head
            while n != None:
                new_list.append(n.data)
                n = n.next
            for e in reversed(new_list):
                print(e)

    def delete_all(self, data):
        if self.__head is None:
            print("List is empty so no element was deleted.")
        else:
            n = self.__head
            n_prev = None
            found = False
            while n != None:
                if n.data == data:
                    if n_prev is None:
                        self.__head = n.next
                    elif n.next is None:
                        n_prev.next = None
                    else:
                        n_prev.next = n.next
                n_prev = n
                n = n.next

    def print_linked_list(self):
        n = self.__head
        if not n:
            print("Linked list is empty")
        else:
            while n != None:
                print(n.data)
                n = n.next

    def size(self):
        count = 0
        if self.__head != None:
            n = self.__head
            while n != None:
                count += 1
                n = n.next
        return count


# %%

#p = PrivateLinkedList()
#p.__head = None
#p.add_last(1)
#p.add_last(2)
#p.__head = None
#p.print_linked_list()

# %%
#Práctica realizada por Alexandre Lacoste Rodríguez, Hafsa El Jauhari Al Jaouhari y Lucía López García

import getopt
import protocols as p
import socket


def command(client_socket):
    valid = False
    cancel_save = False
    filename = None
    while not valid:
        option = input('What do you want to do? ')
        if option == 'a':
            valid = True
        elif option == 's':
            filename, cancel_save = ask_filename_for_saving(filename, cancel_save)
            if not cancel_save:
                valid = True
        else:
            print('Invalid command. Try again (a or s)')
    message = {'header': p.MSG_SEND_COMMAND, 'action': option, 'filename': filename}
    p.send_one_message(client_socket, message)


def manage_choose_game(message, client_socket):
    print(message['message'])
    stop = False
    if message['options_range']:
        option = choose_one_option(message)
        message = {'header': p.MSG_SEND_GAME, 'option': option}
        p.send_one_message(client_socket, message)
    else:
        message = {'header': p.MSG_DC_ME}
        p.send_one_message(client_socket, message)
        stop = True
    return stop


def ask_filename_for_saving(filename, cancel):
    valid = False
    while not valid:
        filename = input("Enter a name for the file: ")
        if not filename.endswith('.json') and not filename.endswith('.txt'):
            if filename == 'cancel':
                cancel = True
                valid = True
                print('The game was not saved')
            else:
                print('Format incorrect. The filename must end with .txt or .json')
        else:
            valid = True
    return filename, cancel


def ask_filename_for_loading():
    valid = False
    while not valid:
        filename = input('Enter the name of the file: ')
        if not filename.endswith('.json') and not filename.endswith('.txt'):
            print('Format incorrect. The filename must end with .txt or .json')
        else:
            valid = True
    return filename


def manage_end_game(message):
    print(message['message'])
    if message['win']:
        print('Congratulations, You won the game!')
    else:
        print('You lost the game. Try again')


def choose_character(message, client_socket):
    print(message['menu'])
    option = choose_one_option(message)
    message = {'header': p.MSG_SEND_CHARACTER, 'option': option}
    p.send_one_message(client_socket, message)


def manage_welcome(message, client_socket, stages, name):
    stop = False
    if message['accepted']:
        print(message['menu'])
        option = choose_one_option(message)
        if option == 3:
            filename = ask_filename_for_loading()
            message = {'header': p.MSG_SEND_OPTION, 'option': option, 'filename': filename}
            p.send_one_message(client_socket, message)
        else:
            message = {'header': p.MSG_SEND_OPTION, 'option': option, 'stages': stages}
            p.send_one_message(client_socket, message)
    else:
        print(f'Access denied. There is someone called {name} playing.')
        stop = True
    return stop


def manage_message(message, client_socket, stages, name):
    header = message['header']
    stop = False
    if header == p.MSG_WELCOME:
        stop = manage_welcome(message, client_socket, stages, name)
    elif header == p.MSG_CHARACTER_MENU:
        choose_character(message, client_socket)
    elif header == p.MSG_SERVER_MSS:
        print(message['message'])
    elif header == p.MSG_LIST_GAMES:
        stop = manage_choose_game(message, client_socket)
    elif header == p.MSG_YOUR_TURN:
        print(message['message'], end=' ')
        command(client_socket)
    elif header == p.MSG_DC_SERVER:
        print(message['reason'])
        stop = True
    elif header == p.MSG_END_GAME:
        manage_end_game(message)
        stop = True
    return stop


def send_join(client_socket, name):
    message = {'header': p.MSG_JOIN, 'name': name}
    p.send_one_message(client_socket, message)


def start(client_socket, name, stages):
    stop = False
    try:
        send_join(client_socket, name)
        while not stop:
            message = p.recv_one_message(client_socket)
            stop = manage_message(message, client_socket, stages, name)
    except KeyboardInterrupt:
        print('Disconnection success.')
        message = {'header': p.MSG_DC_ME}
        p.send_one_message(client_socket, message)
    except p.ConnectionClosed as e:
        print(e)


def choose_one_option(message):
    exit = False
    while not exit:
        try:
            option = int(input('Your option: '))
            if option in message['options_range']:
                exit = True
            else:
                raise ValueError
        except ValueError:
            print(f'Try again. 1 Integer number of these: {message["options_range"]}')
    return option


def parse_args():
    import sys
    opts, args = getopt.getopt(sys.argv[1:], "s:n:i:p:", ["stages=", "name=", "ip=", "port="])
    name = None
    stages = 1
    ip = '127.0.0.1'
    port = 7123
    for o, a in opts:
        if o in ("-n", "--name"):
            name = a
        elif o in ("-s", "--stages"):
            stages = a
        elif o in ("-i", "--ip"):
            ip = a
        elif o in ("-p", "--port"):
            port = a
    return name, stages, ip, port


def check_args(stages, port, name):
    stages_ok = True
    port_ok = True
    name_ok = False
    try:
        number = int(port)
        if number <= 1024:
            raise ValueError
    except ValueError:
            port_ok = False
    try:
        number = int(stages)
        if number < 1 or number > 10:
            raise ValueError
    except ValueError:
        stages_ok = False
    if name is not None or "":
        name_ok =True
    return stages_ok, port_ok, name_ok


try:
    name, stages, ip, port = parse_args()
    stages_ok, port_ok, name_ok = check_args(stages, port, name)
    if stages_ok and port_ok and name_ok:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((ip, int(port)))
        start(client_socket, name, int(stages))
        client_socket.close()
    else:
        if not stages_ok:
            print('Access Denied. Argument Stages(-s) must be an integer between 1-10.')
        if not port_ok:
            print('Access Denied. Argument Port(-p) must be an integer bigger than 1024.')
        if not name_ok:
            print("Access Denied. You must provide a Name(-n), it can't be None")
except TimeoutError:
    print('Invalid IP')
except ConnectionResetError:
    print('Server closed by admin')
except OSError:
    print('Disconnected')
except getopt.GetoptError:
    print("Invalid arguments")
except BrokenPipeError:
    print('The server is closed')
